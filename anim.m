function anim(Y1, p, l, r, u, o)
%   Copyright 2016 Markus Schmid MCI
%%  Plot--Animation 
global n

[a,b]=size(Y1); 
pbaspect([1 1 1])
switch n
    case 0
    figure(1)
    for i=1:1:a  
    hold on
    grid on 
    x1 = Y1(i,1);
    y1 = Y1(i,2);
    plot (x1,y1,'r.')
    x2 = Y1(i,5);
    y2 = Y1(i,6);
    plot (x2,y2,'b.')
    x3 = Y1(i,9);
    y3 = Y1(i,10);
    plot (x3,y3,'g.')
    axis([l r u o])
    drawnow
    end
    title ( 'System Sun-Earth-Moon scaled' )
    legend ('Sun', 'Earth' , 'Moon')
    
    case 1
   figure(1)
     for i=1:1:a  
    hold on
    grid on 
    x1 = Y1(i,1);
    y1 = Y1(i,2);
    plot (x1,y1,'r.')
    x2 = Y1(i,5);
    y2 = Y1(i,6);
    plot (x2,y2,'b.')
    x3 = Y1(i,9);
    y3 = Y1(i,10);
    plot (x3,y3,'g.')
    axis([l r u o])
    drawnow
   % pause(0.1);
     end
    title ( '3-body problem')
    legend ('mass 1', 'mass 2' , 'mass 3')
    
    case 2
        for i=1:1:a  
    figure(1)     
    x1 = Y1(i,1);
    y1 = Y1(i,2);
    plot (x1,y1,'r-o','MarkerSize',25,'MarkerFaceColor',[1,1,0])
    hold on
    grid on 
    x2 = Y1(i,5);
    y2 = Y1(i,6);
    plot (x2,y2,'b-o','MarkerSize',10,'MarkerFaceColor',[0,0,0.5])
    x3 = Y1(i,9);
    y3 = Y1(i,10);
    plot (x3,y3,'g-o','MarkerSize',5,'MarkerFaceColor',[0.5,0.5,0.5])
    axis([l r u o])
    drawnow
      end
    title ( 'System Sun-Earth-Moon unscaled' )
    legend ('Sun', 'Earth' , 'Moon')
    hold off
     
    case 3
    figure(1)
    for i=1:1:a  
    hold on
    grid on 
    x1 = Y1(i,1);
    y1 = Y1(i,2);
    plot (x1,y1,'r.')
    x2 = Y1(i,5);
    y2 = Y1(i,6);
    plot (x2,y2,'b.')
    x3 = Y1(i,9);
    y3 = Y1(i,10);
    plot (x3,y3,'g.')
    axis([l r u o])
    drawnow
    end
    title ( 'equilateral triangle with special initial conditions')
    legend ('mass 1', 'mass 2' , 'mass 3')
    
     case 4
   figure(1)
     for i=1:1:a  
    hold on
    grid on 
    x1 = Y1(i,1);
    y1 = Y1(i,2);
    plot (x1,y1,'r.')
    x2 = Y1(i,5);
    y2 = Y1(i,6);
    plot (x2,y2,'b.')
    x3 = Y1(i,9);
    y3 = Y1(i,10);
    plot (x3,y3,'g.')
    axis([l r u o])
    drawnow
   % pause(0.1);
     end
    title ( '3-body problem')
    legend ('mass 1', 'mass 2' , 'mass 3')
      otherwise
        end    
      