%% 3 body problem in MatLab mit ODE45.
%   Copyright 2016 Markus Schmid MCI
%      F = - G m1 m2 ( x1 - x2 ) / |x1 - x2|^3
%%
%      y1 = p0(x)
%      y2 = p0(y)
%      y3 = p0'(x)
%      y4 = p0'(y)
%
%    12 first order equations
%    Equations 1 - 4
%
%      y1' = y3
%      y2' = y4
%      y3' = - G * m2 ( y1 - y5  ) / |(y1,y2) - (y5,y6) |^3 
%            - G * m3 ( y1 - y9  ) / |(y1,y2) - (y9,y10)|^3
%      y4' = - G * m2 ( y2 - y6  ) / |(y1,y2) - (y5,y6) |^3 
%            - G * m3 ( y2 - y10 ) / |(y1,y2) - (y9,y10)|^3
%
clear
close all
clc
  global m1 m2 m3 G n

  fprintf ( 1, 'Welcome to the 3-body simulator\n ' );
  fprintf ( 1, 'Please choose simulation:\n' );
  fprintf ( 1, '0.: system Sun-Earth-Moon scaled\n' );
  fprintf ( 1, '1.: predefined 3-body problem\n' );
  fprintf ( 1, '2.: system Sun-Earth-Moon not scaled\n' );
  fprintf ( 1, '3.: equilateral triangle with same mass and velocity\n' );
  fprintf ( 1, '4.: common 3-body problem \n' );
  n = input('Please choose: ');
  p = input('\nAnimated mode : 1: ON / 0: OFF   ');

  switch n
   case 0
%% System Sun-Earth-Moon
       disp('System Sun-Earth-Moon')
  m1    = 1.989e30;             % mass Sun
  m2    = 5.972e24;             % mass Earth
  m3    = 7.349e22;             % mass Moon
  G     = 6.67259e-11;          % gravity constant m^3/(kg *s^2)
  
  xes   = 1.496e11;             % distance AU
  xem   = 3.844e8;              % distance Earth - Moon
  xsm   = xes - xem;            % initial condition
  
  SP = (xes * (m1)/(m1+m2));    % distance Earth - common center of gravity (sun,earth)
  SP1 = (xes * (m2)/(m1+m2));   % distance Sun - common center of gravity (sun,earth)
  SP2 = SP + SP1;
  SPm = (xem)*m2/(m2+m3);       % distance Moon - common center of gravity (earth,moon)
  SPm1 = (xem)*m3/(m2+m3);      % distance Earth - common center of gravity (earth,moon)
  
  yr = 1;                                   % years
  t1 = 0;                                   % start time
  T = sqrt((4*pi^2*SP^3)/(G*(m1+m2)));      % period Earth
  Tm = sqrt((4*pi^2*xem^3)/(G*(m3+m2)));    % period moon
  t_final = yr * T;                         % final time
  t_range = linspace(t1, t_final, yr*1000);
  
  % Axes
  l= -1.6*xes;                      % length -x axis
  r=  1.6*xes;                      % 
  u= -1.6*xes;                      % length -y axis
  o=  1.6*xes;                      % 
  
 % Initial position
  X1x = 0.0;                        % Initial x - position body 1
  X1y = 0.0;                        % Initial y - position body 1
  X2x = xes;                        % Initial x - position body 2       
  X2y = 0.0;                        % Initial y - position body 2 
  X3x = xsm;                        % Initial x - position body 3 
  X3y = 0.0;                        % Initial y - position body 3
  
  % Initial Velocity
  V1x = 0.0;
  V1y = -(2*pi/(T))*(xes-SP);
  V2x = 0.0;
  V2y = (2*pi/(T))*SP;
  V3x = 0;
  V3y = V2y + (2*pi/(Tm))*xem;
%           
   case 1
%% Allgemeines 3-K�rperproblem   
       disp('common 3-body problem')
  m1    = 2;                % mass 1
  m2    = 10;               % mass 2 
  m3    = 2;                % mass 3
  G     = 1; 
  % initial points
  X1x = -3;
  X1y = 2;
  X2x = 0;
  X2y = 0;
  X3x = 3;
  X3y = 2;
  
  % initial velocity
  V1x = 1;
  V1y = 0;
  V2x = 0;
  V2y = -0.2;
  V3x = -1;
  V3y = 0;
  
  t_initial = 0.0;
  t_final = input('Select t_final: ');
  resolution = 100;
  t_range = linspace(t_initial, t_final,t_final*resolution);
  
  max_x = max( [X1x, X2x, X3x, X1y, X2y, X3y] );

  l     = -1.5*max_x;
  r     =  1.5*max_x;
  u     = -1.5*max_x;
  o     =  1.5*max_x;
  
%            
    case 2
%% Sonne-Erde-Mond ohne Ma�stab
    disp('Sun-Earth-Moon different scale')
  m1    = 200;
  m2    =  10;
  m3    = 0.01;
  G     = 6.67408e-11;      % gravity constant m^3/(kg *s^2) 
  
  % initial points
  X1x = 0;
  X1y = 0;
  X2x = 10;
  X2y = 0;
  X3x = 9.0;
  X3y = 0.0;
  
  SP = (X2x * (m1)/(m1+m2));            % center of gravity (Earth Sun)
  
  SPm = (X2x - X3x)*m2/(m2+m3);         % center of gravity (Earth Moon)
  
  t_initial = 0.0;
  u = 1.5;                              % periods
  T = sqrt((4*pi^2*SP^3)/(G*(m1+m2)));
  Tm = sqrt((4*pi^2*(SPm)^3)/(G*(m3+m2)));
  t_final = u*T;
  t_range = linspace( t_initial, t_final, u*1000);
  
  % initial velocity
  V1x = 0;
  V1y = -(2*pi/T)*(10-SP);
  V2x = 0;
  V2y = (2*pi/T)*SP;
  V3x = 0;
  %V2y = 8;
  V3y = V2y + (2*pi/Tm)*(SPm);
  
  max_x = max( [X1x, X2x, X3x] );
  max_y = max( [X1y, X2y, X3y] );
  
  % axes
  l     = -1.5*max_x;
  r     =  1.5*max_x;
  u     = -1.5*max_x;
  o     =  1.5*max_x;
   %
    case 3
%% equilateral triangle with same masses
       disp('equilateral triangle')
  m1    = 10;
  m2    = 10;
  m3    = 10;
  G     = 1;  
  
  a = 5;                        % Kantenl�nge 5
  ha=sqrt((a^2-(a/2)^2));
  ri = ha/3;
  
  % intial points
  X1x = -a/2;
  X1y = -ri;
  X2x = a/2;
  X2y = -ri;
  X3x = 0.0;
  X3y = (ha-ri);
  
  % initial velocity

  V1x = 1.0;
  V1y = 0.0;
  V2x = -0.5;
  V2y = sqrt(1^2-V2x^2);
  V3x = -0.5;
  V3y = -sqrt(1^2-V3x^2);


  t_initial = 0.0;
  t_final = input('Select t_final: ');
  resolution = 20;
  t_range = linspace( t_initial, t_final , t_final*resolution);
  
  max_x = max( [X1x, X2x, X3x, X1y, X2y, X3y] );

  l     = -1.5*max_x;
  r     =  1.5*max_x;
  u     = -1.5*max_x;
  o     =  1.5*max_x;
%      
    case 4
%% common 3-body problem   
       disp('common 3-body problem')
  m1    = input('Please choose m1: ');                  % mass 1
  m2    = input('Please choose m2: ');                  % mass 2 
  m3    = input('Please choose m3: ');                  % mass 3
  G     = 1; 
  % initial points
  X1x = input('Please choose x-position m1: ');
  X1y = input('Please choose y-position m1: ');
  X2x = input('Please choose x-position m2: ');
  X2y = input('Please choose y-position m2: ');
  X3x = input('Please choose x-position m3: ');
  X3y = input('Please choose y-position m3: ');
  
  % initial velocity
  V1x = input('Please choose x-velocity m1: ');
  V1y = input('Please choose y-velocity m1: ');
  V2x = input('Please choose x-velocity m2: ');
  V2y = input('Please choose y-velocity m2: ');
  V3x = input('Please choose x-velocity m3: ');
  V3y = input('Please choose y-velocity m3: ');
  
  t_initial = 0.0;
  t_final = input('Select t_final: ');
  resolution = 100;
  t_range = linspace(t_initial, t_final,t_final*resolution);
  
  max_x = max( [X1x, X2x, X3x, X1y, X2y, X3y] );

  l     = -1.5*max_x;
  r     =  1.5*max_x;
  u     = -1.5*max_x;
  o     =  1.5*max_x;
  
%            
      otherwise
  end 
  
%% handover inital position and velocity to ODE45 function
  x_initial = [ X1x;  X1y;   V1x;   V1y;
                X2x;  X2y;   V2x;   V2y;
                X3x;  X3y;   V3x;   V3y];
%%  Error tolerance
  options = odeset ( 'RelTol', 1.0e-10, 'AbsTol', 1.0e-10 );
%%  ODE Funktion
 [ T1, Y1 ] = ode45 (@simple_f, t_range, x_initial, options );
 % Y1=[x(m1),y(m1),vx(m1),vy(m1),x(m2),y(m2),vx(m2),vy(m2),x(m3),y(m3),vx(m3),vy(m3)]
%
%% Call Animation
if p == 1 
anim(Y1, p, l, r, u, o)
end
%
%%  Plots
figure ( 2 )
    hold on
    axis([l r u o])
    plot ( Y1(:,1), Y1(:,2),'r','LineWidth',1)
    plot ( Y1(:,5), Y1(:,6),'b','LineWidth',1 )
    plot ( Y1(:,9), Y1(:,10),'g','LineWidth',1 )
    pbaspect([1 1 1])
    axis square
    grid on
switch n
    case 0
    legend ('Sun', 'Earth' , 'Moon')
    
    case 2
    legend ('Sun', 'Earth' , 'Moon')
    
    otherwise
    legend ('Mass 1', 'Mass 2' , 'Mass 3')
    plot(X1x,X1y,'o','Displayname','Initial Position m1');
    plot(X2x,X2y,'o','Displayname','Initial Position m2');
    plot(X3x,X3y,'o','Displayname','Initial Position m3');
end 
%% function call zero-velocity plot
if n == 0 
zerov(m1, m2, 6) 
else
end
%
%% end of programm
   fprintf ( 1, '  Normal end of execution.\n' );