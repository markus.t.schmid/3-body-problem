function ydot = simple_f ( t, y )
%   Copyright 2016 Markus Schmid MCI
%*************************************************************************
  global m1 m2 m3 G n
  r = 5e-3;
switch n
    case 0                     
  r1 =  6.95e8;                 % radius sun
  r2 =  6.371e6;                % radius earth
  r3 =  1.737e6;                % radius moon

    otherwise
  r1 =  r;
  r2 =  r;
  r3 =  r;
end
                
  x1 = y(1:2);
  x2 = y(5:6);
  x3 = y(9:10);

  ab23 = norm (x3-x2);
  ab13 = norm (x3-x1);
  ab21 = norm (x1-x2);
  
  if ab23<r2+r3                  % Collision detection
      ab23=r2+r3;       
  end 
  
  if ab13<r1+r3                  % Collision detection
      ab13=r1+r3;
  end
  
  if ab21<r1+r2                   % Collision detection
      ab21=r2+r1;
  end
  
  d1 = ( x3 - x2 ) / (ab23^2 * norm (x3-x2));
  d2 = ( x1 - x3 ) / (ab13^2 * norm (x3-x1));
  d3 = ( x2 - x1 ) / (ab21^2 * norm (x1-x2));
  
  ydot(1:2) = y(3:4);
  ydot(3:4) = G * (m2 * d3 - m3 * d2);
  ydot(5:6) = y(7:8);
  ydot(7:8) = G * (m3 * d1 - m1 * d3);
  ydot(9:10) = y(11:12);
  ydot(11:12) = G * (m1 * d2 - m2 * d1);

  ydot = ydot(:);
  return
end
