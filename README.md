**Repository for 3-body simulator**

This code was created as part of a numeric analysis lecture in 2017.

The original MATLAB Version was 2016b
The code was last tested with Matlab 2019b 

I have not tested the Matlab versions in between.

To run the script execute the main.m file with all the other files in the same working directory and follow the instructions on the Matlab terminal.

At the moment the project consits of
- system Sun-Earth-Moon scaled
- predefined 3-body problem
- system Sun-Earth-Moon not scaled
- equilateral triangle with same mass and velocity
- common 3-body problem

An (unsophisticated) animation is also availabe and can be selected with Animated Mode = 1 

The common 3-body problem prompts for input data:
- mass m1
- mass m2
- mass m3
- x coordinate m1
- y coordinate m1
- x coordinate m2
- y coordinate m2
- x coordinate m3
- y coordinate m4
- intial velocity in x direction m1
- intial velocity in y direction m1
- intial velocity in x direction m2
- intial velocity in y direction m2
- intial velocity in x direction m3
- intial velocity in y direction m3
- t_final duration of simulation

Have Fun!

Markus 


